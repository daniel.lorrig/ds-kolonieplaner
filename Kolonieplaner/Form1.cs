﻿using KolonieplanerCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kolonieplaner
{
    public partial class KolonieplanerForm : Form
    {
        Asteroid _asteroid;
        Dictionary<string, Label> _productionLabels = new Dictionary<string, Label>();
        Dictionary<string, Label> _fabricProductionLabels = new Dictionary<string, Label>();
        Dictionary<string, Label> _costLabels = new Dictionary<string, Label>();
        Dictionary<TextBox, string> _oreRefineryAmountToRessource = new Dictionary<TextBox, string>();
        Dictionary<TextBox, string> _fabrikAmountToRessource = new Dictionary<TextBox, string>();

        public KolonieplanerForm()
        {
            InitializeComponent();
            CreateFields();
            CreateErzverarbeiterView();
            CreateMunitionFabrikView();
            CreateModuleFabrikView();
            CreateItemsFabrikView();
            CreateBuildingListbox();
            CreateRessourceView();
            CreateFabricProductionView();
        }

        private void CreateRessourceView()
        {
            int distanceY = 33;

            int i = 0;
            foreach (var ressource in _asteroid.RessourceInfos.Where(x => x.Value.Kategorie == "Rohstoff"))
            {

                var label = GetAmountLabel(i);
                this.tabPage1.Controls.Add(label);
                _productionLabels.Add(ressource.Key, label);

                label = GetAmountLabel(i);
                this.tabPage2.Controls.Add(label);
                _costLabels.Add(ressource.Key, label);


                var labelText = GetNameLabel(i, ressource.Key);
                this.tabPage1.Controls.Add(labelText);
                labelText = GetNameLabel(i, ressource.Key);
                this.tabPage2.Controls.Add(labelText);

                var pictureBox = GetPictureBox(i, ressource.Value);
                this.tabPage1.Controls.Add(pictureBox);
                pictureBox = GetPictureBox(i, ressource.Value);
                this.tabPage2.Controls.Add(pictureBox);

                i++;
            }
        }

        private void CreateFabricProductionView()
        {
            int i = 0;
            foreach (var ressource in _asteroid.RessourceInfos.Where(x => x.Value.Kategorie == "Munition" ||x.Value.Kategorie == "Modul" ||x.Value.Kategorie == "Item"))
            {
                //_fabricProductionLabels

                var label = GetAmountLabel(i);
                label.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
                this.tpFabrikProduktion.Controls.Add(label);
                _fabricProductionLabels.Add(ressource.Key, label);

                var labelText = GetNameLabel(i, ressource.Key);
                labelText.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
                this.tpFabrikProduktion.Controls.Add(labelText);

                var pictureBox = GetPictureBox(i, ressource.Value);
                this.tpFabrikProduktion.Controls.Add(pictureBox);

                i++;
            }
        }

        private void CreateErzverarbeiterView()
        {
            int move = 15;
            var xPositions = new int[]
            {
                30, // Ressource picture
                105, // Amount
                
                180 + move, // time picture
                205 + move, // time label
                
                270 + move, // Ore picture
                280 + move // Amount of Ore
                //190 // amount of ressource per cost - not really needed, as this is always 1
            };

            var timeDummyRessource = new Ressource() { IsErz = false, Name = "Time", ImgPath = "./img/Ressources/Zeit.png" };
            int counter = 1;
            foreach (var erzprodukt in _asteroid.RessourceInfos.Values.Where(x => x.IsErz).ToList())
            {
                var productionPicture = GetPictureBox(counter, erzprodukt);
                productionPicture.Location = new Point(xPositions[0], productionPicture.Location.Y);

                var amountTextbox = GetTextbox(counter);
                amountTextbox.Text = "0";
                amountTextbox.Location = new Point(xPositions[1], amountTextbox.Location.Y);
                amountTextbox.TextChanged += tbErzAmountTextChanged;
                _oreRefineryAmountToRessource.Add(amountTextbox, erzprodukt.Name);

                var timePicture = GetPictureBox(counter, timeDummyRessource);
                timePicture.Location = new Point(xPositions[2], timePicture.Location.Y);

                var timeLabel = GetAmountLabel(counter);
                timeLabel.Location = new Point(xPositions[3], timeLabel.Location.Y);
                timeLabel.Text = erzprodukt.ErzverarbeitungRessource.Zeit.ToString();


                var costPicture = GetPictureBox(counter, _asteroid.RessourceInfos["Erz"]);
                costPicture.Location = new Point(xPositions[4], costPicture.Location.Y);
                var costLabel = GetAmountLabel(counter);
                costLabel.Location = new Point(xPositions[5], costLabel.Location.Y);
                costLabel.Text = (-1 * erzprodukt.ErzverarbeitungRessource.Produktion["Erz"]).ToString();


                tpErzverarbeiter.Controls.Add(amountTextbox);
                tpErzverarbeiter.Controls.Add(timePicture);
                tpErzverarbeiter.Controls.Add(timeLabel);
                tpErzverarbeiter.Controls.Add(costPicture);
                tpErzverarbeiter.Controls.Add(costLabel);
                tpErzverarbeiter.Controls.Add(productionPicture);

                counter++;
            }
        }

        private void CreateMunitionFabrikView()
        {
            int move = 15;
            var xPositions = new int[]
            {
                30, // Ressource picture
                105, // Amount
                
                180 + move, // time picture
                205 + move, // time label
                
                270 + move, // Ore picture
                280 + move // Amount of Ore
            };

            var timeDummyRessource = new Ressource() { IsErz = false, Name = "Time", ImgPath = "./img/Ressources/Zeit.png" };
            int counter = 1;
            foreach (var munitionstyp in _asteroid.RessourceInfos.Values.Where(x => x.Kategorie == "Munition").ToList())
            {
                var productionPicture = GetPictureBox(counter, munitionstyp, 30);
                productionPicture.Location = new Point(xPositions[0], productionPicture.Location.Y);

                var amountTextbox = GetTextbox(counter, 30);
                amountTextbox.Text = "0";
                amountTextbox.Location = new Point(xPositions[1], amountTextbox.Location.Y);
                amountTextbox.TextChanged += tbFabrikAmountTextChanged;
                _fabrikAmountToRessource.Add(amountTextbox, munitionstyp.Name);

                var timePicture = GetPictureBox(counter, timeDummyRessource, 30);
                timePicture.Location = new Point(xPositions[2], timePicture.Location.Y);

                var timeLabel = GetAmountLabel(counter, 30);
                timeLabel.Location = new Point(xPositions[3], timeLabel.Location.Y);
                timeLabel.Text = munitionstyp.ErzverarbeitungRessource.Zeit.ToString();

                tpMunition.Controls.Add(amountTextbox);
                tpMunition.Controls.Add(timePicture);
                tpMunition.Controls.Add(timeLabel);
                tpMunition.Controls.Add(productionPicture);

                counter++;
            }
        }

        private void CreateModuleFabrikView()
        {
            int move = 15;
            var xPositions = new int[]
            {
                30, // Ressource picture
                105, // Amount
                
                180 + move, // time picture
                205 + move, // time label
                
                270 + move, // Ore picture
                280 + move // Amount of Ore
            };

            var timeDummyRessource = new Ressource() { IsErz = false, Name = "Time", ImgPath = "./img/Ressources/Zeit.png" };
            int counter = 1;
            foreach (var munitionstyp in _asteroid.RessourceInfos.Values.Where(x => x.Kategorie == "Modul").ToList())
            {
                var productionPicture = GetPictureBox(counter, munitionstyp, 30);
                productionPicture.Location = new Point(xPositions[0], productionPicture.Location.Y);

                var amountTextbox = GetTextbox(counter, 30);
                amountTextbox.Text = "0";
                amountTextbox.Location = new Point(xPositions[1], amountTextbox.Location.Y);
                amountTextbox.TextChanged += tbFabrikAmountTextChanged;
                _fabrikAmountToRessource.Add(amountTextbox, munitionstyp.Name);

                var timePicture = GetPictureBox(counter, timeDummyRessource, 30);
                timePicture.Location = new Point(xPositions[2], timePicture.Location.Y);

                var timeLabel = GetAmountLabel(counter, 30);
                timeLabel.Location = new Point(xPositions[3], timeLabel.Location.Y);
                timeLabel.Text = munitionstyp.ErzverarbeitungRessource.Zeit.ToString();

                tpModule.Controls.Add(amountTextbox);
                tpModule.Controls.Add(timePicture);
                tpModule.Controls.Add(timeLabel);
                tpModule.Controls.Add(productionPicture);

                counter++;
            }
        }

        private void CreateItemsFabrikView()
        {
            int move = 15;
            var xPositions = new int[]
            {
                30, // Ressource picture
                105, // Amount
                
                180 + move, // time picture
                205 + move, // time label
                
                270 + move, // Ore picture
                280 + move // Amount of Ore
            };

            var timeDummyRessource = new Ressource() { IsErz = false, Name = "Time", ImgPath = "./img/Ressources/Zeit.png" };
            int counter = 1;
            foreach (var munitionstyp in _asteroid.RessourceInfos.Values.Where(x => x.Kategorie == "Item").ToList())
            {
                var productionPicture = GetPictureBox(counter, munitionstyp, 30);
                productionPicture.Location = new Point(xPositions[0], productionPicture.Location.Y);

                var amountTextbox = GetTextbox(counter, 30);
                amountTextbox.Text = "0";
                amountTextbox.Location = new Point(xPositions[1], amountTextbox.Location.Y);
                amountTextbox.TextChanged += tbFabrikAmountTextChanged;
                _fabrikAmountToRessource.Add(amountTextbox, munitionstyp.Name);

                var timePicture = GetPictureBox(counter, timeDummyRessource, 30);
                timePicture.Location = new Point(xPositions[2], timePicture.Location.Y);

                var timeLabel = GetAmountLabel(counter, 30);
                timeLabel.Location = new Point(xPositions[3], timeLabel.Location.Y);
                timeLabel.Text = munitionstyp.ErzverarbeitungRessource.Zeit.ToString();

                tpItems.Controls.Add(amountTextbox);
                tpItems.Controls.Add(timePicture);
                tpItems.Controls.Add(timeLabel);
                tpItems.Controls.Add(productionPicture);

                counter++;
            }
        }

        private void tbErzAmountTextChanged(object sender, EventArgs e)
        {
            var tbSender = (TextBox)sender;
            if (string.IsNullOrWhiteSpace(tbSender.Text))
            {
                _asteroid.Erzverarbeitung.ChangeProductionValue(_oreRefineryAmountToRessource[tbSender], 0);
            }
            else
            {
                _asteroid.Erzverarbeitung.ChangeProductionValue(_oreRefineryAmountToRessource[tbSender], Int32.Parse(tbSender.Text));
            }

            UpdateProduction();
        }

        private void tbFabrikAmountTextChanged(object sender, EventArgs e)
        {
            var tbSender = (TextBox)sender;
            if (string.IsNullOrWhiteSpace(tbSender.Text))
            {
                _asteroid.Fabrik.ChangeProductionValue(_fabrikAmountToRessource[tbSender], 0);
            }
            else
            {
                _asteroid.Fabrik.ChangeProductionValue(_fabrikAmountToRessource[tbSender], Int32.Parse(tbSender.Text));
            }

            UpdateProduction();
        }

        private Label GetAmountLabel(int index, int distanceY = 35)
        {
            var label = new Label();
            label.Anchor = ((AnchorStyles)((AnchorStyles.Top | AnchorStyles.Right)));
            label.AutoSize = true;
            label.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label.ForeColor = SystemColors.HotTrack;
            label.Location = new Point(176, 5 + index * distanceY);
            label.MinimumSize = new Size(50, 30);
            label.Size = new Size(50, 30);
            label.Text = "0";
            label.TextAlign = ContentAlignment.MiddleRight;

            return label;
        }
        private Label GetNameLabel(int index, string ressource, int distanceY = 35)
        {
            var label = new Label();
            label.AutoSize = true;
            label.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label.ForeColor = SystemColors.MenuHighlight;
            label.Location = new Point(36, 5 + index * distanceY);
            label.Margin = new Padding(3);
            label.MinimumSize = new Size(0, 30);
            label.Size = new Size(72, 30);
            label.TabIndex = 11;
            label.Text = ressource;
            label.TextAlign = ContentAlignment.MiddleCenter;

            return label;
        }
        private PictureBox GetPictureBox(int index, Ressource ressource, int distanceY = 35)
        {
            var pictureBox = new PictureBox();
            pictureBox.Image = Image.FromFile(ressource.ImgPath);
            pictureBox.Location = new Point(0, 5 + index * distanceY);
            pictureBox.Size = new Size(30, 30);
            pictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
            pictureBox.TabIndex = 0;
            pictureBox.TabStop = false;

            return pictureBox;
        }

        private TextBox GetTextbox(int index, int distanceY = 35)
        {
            
            var textbox = new TextBox();
            textbox.Location = new System.Drawing.Point(58, 5 + index * distanceY);
            textbox.Size = new System.Drawing.Size(50, 23);
            textbox.Text = "0";

            return textbox;
        }
        private void CreateBuildingListbox()
        {
            var buildingsWithoutCores = _asteroid.BuildingInfos.Values
                .Where(x => x.Kategorie != "Core")
                .OrderBy(y => y.Name != "Empty")
                .ThenBy(y =>  y.Name)
                .Select(x => x.Name)
                .ToArray();
            buildingListbox.Items.AddRange(buildingsWithoutCores);

            buildingListbox.SelectedIndex = 0;

            cbCore.Items.Add("Empty");
            cbCore.Items.AddRange(_asteroid.BuildingInfos.Values.Where(x => x.Kategorie == "Core").Select(x => x.Name).ToArray());
        }

        private void CreateFields()
        {
            JsonParser parser = new JsonParser();
            int numCols = Int32.Parse(tbWidth.Text);
            int numRows = Int32.Parse(tbHeight.Text);

            _asteroid = new Asteroid(parser.BuildingInfos, parser.Ressources, numRows, numCols);
            FelderPanel.Controls.Clear();

            for (int row = 0; row < _asteroid.NumRows; row++)
            {
                for (int col = 0; col < _asteroid.NumCols; col++)
                {
                    var field = CreatePictureBox(row, col);
                    FelderPanel.Controls.Add(field);

                    field.Click += (sender, EventArgs) =>
                    {
                        ChangeBuilding((TransparentControl)sender);
                    };
                }
            }
        }
        void ChangeBuilding(TransparentControl field)
        {
            _asteroid.ChangeBuilding(field.FieldNumber, buildingListbox.SelectedItem.ToString());
            field.Image = Image.FromFile(_asteroid.GetImgPath(field.FieldNumber));

            UpdateProduction();
        }

        private TransparentControl CreatePictureBox(int row, int col)
        {
            int height = 43;
            int width = 49;

            int xDistance = width - 10;

            int moveY = (col % 2) * 42 / 2;

            var pictureBox = new TransparentControl();
            pictureBox.Size = new Size(width, height);
            pictureBox.Location = new Point(xDistance * col, height * row + moveY);
            pictureBox.FieldNumber = row * _asteroid.NumCols + col;

            //pictureBox.Image = global::Kolonieplaner.Properties.Resources.ground0;
            pictureBox.Image = Image.FromFile(_asteroid.Buildings[pictureBox.FieldNumber].GetImgPath());

            return pictureBox;
        }


        private void UpdateProduction()
        {

            foreach (var ressource in _asteroid.RessourceInfos.Where(x => x.Value.Kategorie == "Rohstoff"))
            {
                decimal production = _asteroid.GetRessourceProduction(ressource.Key);
                int cost = 0;

                foreach (var building in _asteroid.Buildings)
                {
                    //production += building.GetProduction(ressource.Key);
                    cost += building.GetCosts(ressource.Key);
                }

                _productionLabels[ressource.Key].Text = production.ToString();
                _costLabels[ressource.Key].Text = cost.ToString();
            }

            foreach (var ressource in _asteroid.RessourceInfos.Where(x => x.Value.Kategorie == "Munition" || x.Value.Kategorie == "Modul" || x.Value.Kategorie == "Item"))
            {
                decimal production = _asteroid.Fabrik.Production(ressource.Key);

                _fabricProductionLabels[ressource.Key].Text = production.ToString();
            }


            lbErzverarbeiterAuslastung.Text = _asteroid.Erzverarbeitung.GetAuslastung() + "/" + _asteroid.Erzverarbeitung.Capacity;
            lbFabrikMunition.Text = _asteroid.Fabrik.GetAuslastung() + "/" + _asteroid.Fabrik.Capacity;
            lbFabrikModule.Text = lbFabrikMunition.Text;
            lbFabrikItems.Text = lbFabrikMunition.Text;


            lbProductionValue.Text = _asteroid.GetProduktionValue().ToString() + "RE";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreateFields();
        }

        private void cbCore_SelectedIndexChanged(object sender, EventArgs e)
        {
            var debug = cbCore.SelectedItem.ToString();
            _asteroid.ChangeCore(debug);

            UpdateProduction();
        }
    }
}
