﻿
namespace Kolonieplaner
{
    partial class KolonieplanerForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FelderPanel = new System.Windows.Forms.Panel();
            this.buildingListbox = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbProductionValue = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tpFabrikProduktion = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.lbErzverarbeiterAuslastung = new System.Windows.Forms.Label();
            this.tpErzverarbeiter = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tpMunition = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbFabrikMunition = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tpModule = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbFabrikModule = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.tpItems = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.lbFabrikItems = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.tbWidth = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbCore = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tpErzverarbeiter.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tpMunition.SuspendLayout();
            this.tpModule.SuspendLayout();
            this.tpItems.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FelderPanel
            // 
            this.FelderPanel.AutoScroll = true;
            this.FelderPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FelderPanel.Location = new System.Drawing.Point(287, 12);
            this.FelderPanel.Name = "FelderPanel";
            this.FelderPanel.Size = new System.Drawing.Size(624, 537);
            this.FelderPanel.TabIndex = 1;
            // 
            // buildingListbox
            // 
            this.buildingListbox.BackColor = System.Drawing.SystemColors.Desktop;
            this.buildingListbox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buildingListbox.FormattingEnabled = true;
            this.buildingListbox.ItemHeight = 15;
            this.buildingListbox.Location = new System.Drawing.Point(930, 95);
            this.buildingListbox.Name = "buildingListbox";
            this.buildingListbox.Size = new System.Drawing.Size(218, 454);
            this.buildingListbox.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label12.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label12.Location = new System.Drawing.Point(63, 390);
            this.label12.Margin = new System.Windows.Forms.Padding(3);
            this.label12.MinimumSize = new System.Drawing.Size(0, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 30);
            this.label12.TabIndex = 31;
            this.label12.Text = "Isochips";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label13.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label13.Location = new System.Drawing.Point(63, 318);
            this.label13.Margin = new System.Windows.Forms.Padding(3);
            this.label13.MinimumSize = new System.Drawing.Size(0, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 30);
            this.label13.TabIndex = 30;
            this.label13.Text = "Xentronium";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label14.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label14.Location = new System.Drawing.Point(63, 282);
            this.label14.Margin = new System.Windows.Forms.Padding(3);
            this.label14.MinimumSize = new System.Drawing.Size(0, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 30);
            this.label14.TabIndex = 29;
            this.label14.Text = "Silizium";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label15.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label15.Location = new System.Drawing.Point(63, 246);
            this.label15.Margin = new System.Windows.Forms.Padding(3);
            this.label15.MinimumSize = new System.Drawing.Size(0, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 30);
            this.label15.TabIndex = 28;
            this.label15.Text = "Adamantium";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label16.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label16.Location = new System.Drawing.Point(63, 210);
            this.label16.Margin = new System.Windows.Forms.Padding(3);
            this.label16.MinimumSize = new System.Drawing.Size(0, 30);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 30);
            this.label16.TabIndex = 27;
            this.label16.Text = "Antimaterie";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label17.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label17.Location = new System.Drawing.Point(63, 174);
            this.label17.Margin = new System.Windows.Forms.Padding(3);
            this.label17.MinimumSize = new System.Drawing.Size(0, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 30);
            this.label17.TabIndex = 26;
            this.label17.Text = "Uran";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label18.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label18.Location = new System.Drawing.Point(63, 138);
            this.label18.Margin = new System.Windows.Forms.Padding(3);
            this.label18.MinimumSize = new System.Drawing.Size(0, 30);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 30);
            this.label18.TabIndex = 25;
            this.label18.Text = "Titan";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(63, 102);
            this.label19.Margin = new System.Windows.Forms.Padding(3);
            this.label19.MinimumSize = new System.Drawing.Size(0, 30);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 30);
            this.label19.TabIndex = 24;
            this.label19.Text = "Kunststoff";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label20.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label20.Location = new System.Drawing.Point(63, 66);
            this.label20.Margin = new System.Windows.Forms.Padding(3);
            this.label20.MinimumSize = new System.Drawing.Size(0, 30);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 30);
            this.label20.TabIndex = 23;
            this.label20.Text = "Deuterium";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label21.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label21.Location = new System.Drawing.Point(63, 30);
            this.label21.Margin = new System.Windows.Forms.Padding(3);
            this.label21.MinimumSize = new System.Drawing.Size(0, 30);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 30);
            this.label21.TabIndex = 22;
            this.label21.Text = "Nahrung";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tpFabrikProduktion);
            this.tabControl1.Location = new System.Drawing.Point(13, 136);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(253, 801);
            this.tabControl1.TabIndex = 35;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Black;
            this.tabPage1.Controls.Add(this.lbProductionValue);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(245, 773);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Produktion";
            // 
            // lbProductionValue
            // 
            this.lbProductionValue.AutoSize = true;
            this.lbProductionValue.BackColor = System.Drawing.Color.Black;
            this.lbProductionValue.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbProductionValue.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbProductionValue.Location = new System.Drawing.Point(3, 755);
            this.lbProductionValue.MinimumSize = new System.Drawing.Size(70, 15);
            this.lbProductionValue.Name = "lbProductionValue";
            this.lbProductionValue.Size = new System.Drawing.Size(70, 15);
            this.lbProductionValue.TabIndex = 0;
            this.lbProductionValue.Text = "0RE";
            this.lbProductionValue.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Black;
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(245, 773);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Baukosten";
            // 
            // tpFabrikProduktion
            // 
            this.tpFabrikProduktion.AutoScroll = true;
            this.tpFabrikProduktion.BackColor = System.Drawing.Color.Black;
            this.tpFabrikProduktion.Location = new System.Drawing.Point(4, 24);
            this.tpFabrikProduktion.Name = "tpFabrikProduktion";
            this.tpFabrikProduktion.Size = new System.Drawing.Size(245, 773);
            this.tpFabrikProduktion.TabIndex = 2;
            this.tpFabrikProduktion.Text = "Fabrik";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(479, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "Auslastung";
            // 
            // lbErzverarbeiterAuslastung
            // 
            this.lbErzverarbeiterAuslastung.AutoSize = true;
            this.lbErzverarbeiterAuslastung.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbErzverarbeiterAuslastung.Location = new System.Drawing.Point(561, 10);
            this.lbErzverarbeiterAuslastung.Name = "lbErzverarbeiterAuslastung";
            this.lbErzverarbeiterAuslastung.Size = new System.Drawing.Size(24, 15);
            this.lbErzverarbeiterAuslastung.TabIndex = 3;
            this.lbErzverarbeiterAuslastung.Text = "0/0";
            // 
            // tpErzverarbeiter
            // 
            this.tpErzverarbeiter.AutoScroll = true;
            this.tpErzverarbeiter.BackColor = System.Drawing.Color.Black;
            this.tpErzverarbeiter.Controls.Add(this.label6);
            this.tpErzverarbeiter.Controls.Add(this.label5);
            this.tpErzverarbeiter.Controls.Add(this.label4);
            this.tpErzverarbeiter.Controls.Add(this.lbErzverarbeiterAuslastung);
            this.tpErzverarbeiter.Controls.Add(this.label3);
            this.tpErzverarbeiter.Location = new System.Drawing.Point(4, 24);
            this.tpErzverarbeiter.Name = "tpErzverarbeiter";
            this.tpErzverarbeiter.Padding = new System.Windows.Forms.Padding(3);
            this.tpErzverarbeiter.Size = new System.Drawing.Size(616, 353);
            this.tpErzverarbeiter.TabIndex = 0;
            this.tpErzverarbeiter.Text = "Erzverarbeiter";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(23, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Produkt";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(108, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Menge";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(244, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Stückkosten";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tpErzverarbeiter);
            this.tabControl2.Controls.Add(this.tpMunition);
            this.tabControl2.Controls.Add(this.tpModule);
            this.tabControl2.Controls.Add(this.tpItems);
            this.tabControl2.Location = new System.Drawing.Point(287, 556);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(624, 381);
            this.tabControl2.TabIndex = 36;
            // 
            // tpMunition
            // 
            this.tpMunition.AutoScroll = true;
            this.tpMunition.BackColor = System.Drawing.Color.Black;
            this.tpMunition.Controls.Add(this.label9);
            this.tpMunition.Controls.Add(this.label10);
            this.tpMunition.Controls.Add(this.label11);
            this.tpMunition.Controls.Add(this.lbFabrikMunition);
            this.tpMunition.Controls.Add(this.label23);
            this.tpMunition.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tpMunition.Location = new System.Drawing.Point(4, 24);
            this.tpMunition.Name = "tpMunition";
            this.tpMunition.Padding = new System.Windows.Forms.Padding(3);
            this.tpMunition.Size = new System.Drawing.Size(616, 353);
            this.tpMunition.TabIndex = 1;
            this.tpMunition.Text = "Fabrik (Munition)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point(477, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 7;
            this.label9.Text = "Auslastung";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Location = new System.Drawing.Point(22, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 15);
            this.label10.TabIndex = 6;
            this.label10.Text = "Produkt";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Location = new System.Drawing.Point(107, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 15);
            this.label11.TabIndex = 5;
            this.label11.Text = "Menge";
            // 
            // lbFabrikMunition
            // 
            this.lbFabrikMunition.AutoSize = true;
            this.lbFabrikMunition.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbFabrikMunition.Location = new System.Drawing.Point(559, 16);
            this.lbFabrikMunition.Name = "lbFabrikMunition";
            this.lbFabrikMunition.Size = new System.Drawing.Size(24, 15);
            this.lbFabrikMunition.TabIndex = 8;
            this.lbFabrikMunition.Text = "0/0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label23.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label23.Location = new System.Drawing.Point(243, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 15);
            this.label23.TabIndex = 4;
            this.label23.Text = "Stückkosten";
            // 
            // tpModule
            // 
            this.tpModule.AutoScroll = true;
            this.tpModule.BackColor = System.Drawing.Color.Black;
            this.tpModule.Controls.Add(this.label22);
            this.tpModule.Controls.Add(this.label24);
            this.tpModule.Controls.Add(this.label25);
            this.tpModule.Controls.Add(this.lbFabrikModule);
            this.tpModule.Controls.Add(this.label27);
            this.tpModule.Location = new System.Drawing.Point(4, 24);
            this.tpModule.Name = "tpModule";
            this.tpModule.Size = new System.Drawing.Size(616, 353);
            this.tpModule.TabIndex = 2;
            this.tpModule.Text = "Fabrik (Module)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label22.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label22.Location = new System.Drawing.Point(477, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 15);
            this.label22.TabIndex = 12;
            this.label22.Text = "Auslastung";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label24.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label24.Location = new System.Drawing.Point(22, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 15);
            this.label24.TabIndex = 11;
            this.label24.Text = "Produkt";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label25.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label25.Location = new System.Drawing.Point(107, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(46, 15);
            this.label25.TabIndex = 10;
            this.label25.Text = "Menge";
            // 
            // lbFabrikModule
            // 
            this.lbFabrikModule.AutoSize = true;
            this.lbFabrikModule.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbFabrikModule.Location = new System.Drawing.Point(559, 16);
            this.lbFabrikModule.Name = "lbFabrikModule";
            this.lbFabrikModule.Size = new System.Drawing.Size(24, 15);
            this.lbFabrikModule.TabIndex = 13;
            this.lbFabrikModule.Text = "0/0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label27.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label27.Location = new System.Drawing.Point(243, 16);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 15);
            this.label27.TabIndex = 9;
            this.label27.Text = "Stückkosten";
            // 
            // tpItems
            // 
            this.tpItems.AutoScroll = true;
            this.tpItems.BackColor = System.Drawing.Color.Black;
            this.tpItems.Controls.Add(this.label26);
            this.tpItems.Controls.Add(this.label28);
            this.tpItems.Controls.Add(this.label29);
            this.tpItems.Controls.Add(this.lbFabrikItems);
            this.tpItems.Controls.Add(this.label31);
            this.tpItems.Location = new System.Drawing.Point(4, 24);
            this.tpItems.Name = "tpItems";
            this.tpItems.Size = new System.Drawing.Size(616, 353);
            this.tpItems.TabIndex = 3;
            this.tpItems.Text = "Fabrik (Items)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label26.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label26.Location = new System.Drawing.Point(477, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 15);
            this.label26.TabIndex = 17;
            this.label26.Text = "Auslastung";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label28.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label28.Location = new System.Drawing.Point(22, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(52, 15);
            this.label28.TabIndex = 16;
            this.label28.Text = "Produkt";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label29.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label29.Location = new System.Drawing.Point(107, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(46, 15);
            this.label29.TabIndex = 15;
            this.label29.Text = "Menge";
            // 
            // lbFabrikItems
            // 
            this.lbFabrikItems.AutoSize = true;
            this.lbFabrikItems.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbFabrikItems.Location = new System.Drawing.Point(559, 16);
            this.lbFabrikItems.Name = "lbFabrikItems";
            this.lbFabrikItems.Size = new System.Drawing.Size(24, 15);
            this.lbFabrikItems.TabIndex = 18;
            this.lbFabrikItems.Text = "0/0";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label31.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label31.Location = new System.Drawing.Point(243, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(77, 15);
            this.label31.TabIndex = 14;
            this.label31.Text = "Stückkosten";
            // 
            // tbHeight
            // 
            this.tbHeight.Location = new System.Drawing.Point(82, 45);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(148, 23);
            this.tbHeight.TabIndex = 38;
            this.tbHeight.Text = "5";
            // 
            // tbWidth
            // 
            this.tbWidth.Location = new System.Drawing.Point(82, 14);
            this.tbWidth.Name = "tbWidth";
            this.tbWidth.Size = new System.Drawing.Size(148, 23);
            this.tbWidth.TabIndex = 37;
            this.tbWidth.Text = "8";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(18, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 15);
            this.label1.TabIndex = 39;
            this.label1.Text = "Breite";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(18, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 15);
            this.label2.TabIndex = 40;
            this.label2.Text = "Höhe";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(82, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 22);
            this.button1.TabIndex = 39;
            this.button1.Text = "Übernehmen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tbWidth);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.tbHeight);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(3);
            this.panel1.Size = new System.Drawing.Size(254, 110);
            this.panel1.TabIndex = 42;
            // 
            // cbCore
            // 
            this.cbCore.FormattingEnabled = true;
            this.cbCore.Location = new System.Drawing.Point(929, 32);
            this.cbCore.Name = "cbCore";
            this.cbCore.Size = new System.Drawing.Size(218, 23);
            this.cbCore.TabIndex = 43;
            this.cbCore.SelectedIndexChanged += new System.EventHandler(this.cbCore_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(930, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 15);
            this.label7.TabIndex = 44;
            this.label7.Text = "Core:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Location = new System.Drawing.Point(929, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 15);
            this.label8.TabIndex = 45;
            this.label8.Text = "Gebäude:";
            // 
            // KolonieplanerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowText;
            this.ClientSize = new System.Drawing.Size(1216, 961);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbCore);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.buildingListbox);
            this.Controls.Add(this.FelderPanel);
            this.Name = "KolonieplanerForm";
            this.Text = "Drifting Souls Kolonieplaner";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tpErzverarbeiter.ResumeLayout(false);
            this.tpErzverarbeiter.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tpMunition.ResumeLayout(false);
            this.tpMunition.PerformLayout();
            this.tpModule.ResumeLayout(false);
            this.tpModule.PerformLayout();
            this.tpItems.ResumeLayout(false);
            this.tpItems.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel FelderPanel;
        private System.Windows.Forms.ListBox buildingListbox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tpErzverarbeiter;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TextBox tbHeight;
        private System.Windows.Forms.TextBox tbWidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbErzverarbeiterAuslastung;
        private System.Windows.Forms.ComboBox cbCore;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbProductionValue;
        private System.Windows.Forms.TabPage tpMunition;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbFabrikMunition;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage tpModule;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbFabrikModule;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabPage tpItems;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label lbFabrikItems;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TabPage tpFabrikProduktion;
    }
}

