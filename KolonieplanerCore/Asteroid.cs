﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KolonieplanerCore
{
    public class Asteroid
    {
        public Asteroid(Dictionary<string, BuildingInfo> buildingInfos, Dictionary<string, Ressource> ressourceInfos, int numRows = 10, int numCols = 5)
        {
            NumRows = Math.Min(numRows, 25);
            NumCols = Math.Min(numCols, 25);

            BuildingInfos = buildingInfos;
            RessourceInfos = ressourceInfos;

            Buildings = new Building[NumRows * NumCols];

            Buildings[0] = new Building(0, BuildingInfos["Kommandozentrale"]);
            for (int i = 1; i < Buildings.Length; i++)
            {
                Buildings[i] = new Building(i);
            }

            Erzverarbeitung = new Erzverarbeitung(ressourceInfos.Values.Where(x => x.IsErz).ToDictionary(x => x.Name, y => y)) { Capacity = 0 };
            Fabrik = new Fabrik(ressourceInfos.Values.Where(x => x.Kategorie == "Munition" ||x.Kategorie == "Item" || x.Kategorie == "Modul").ToDictionary(x => x.Name, y => y)) { Capacity = 0 };
            Core = new Building(-1, buildingInfos["Empty"]);
        }
        public int NumRows;
        public int NumCols;

        public decimal GetRessourceProduction(string ressource)
        {
            int production = 0;

            foreach (var building in Buildings)
            {
                production += building.GetProduction(ressource);
            }

            return production + Erzverarbeitung.Production(ressource) + Core.GetProduction(ressource) + Fabrik.Production(ressource);
        }


        public void ChangeBuilding(int fieldNumber, string newBuilding)
        {
            var oldBuilding = Buildings[fieldNumber];

            if (BuildingInfos[newBuilding].IsUnterirdisch && NumberOfUnterirdisch == MaxUnterirdisch)
            {
                if (!oldBuilding.BuildingInfo.IsUnterirdisch)
                {
                    return;
                }
            }
            if (BuildingInfos[newBuilding].IsUnterirdisch)
            {
                NumberOfUnterirdisch++;
            }

            if (oldBuilding.BuildingInfo.IsUnterirdisch)
            {
                NumberOfUnterirdisch--;
            }

            Erzverarbeitung.Capacity -= oldBuilding.BuildingInfo.Erzfabrik;
            Fabrik.Capacity -= oldBuilding.BuildingInfo.Fabrik;

            Buildings[fieldNumber].ChangeBuilding(BuildingInfos[newBuilding]);
            Erzverarbeitung.Capacity += Buildings[fieldNumber].BuildingInfo.Erzfabrik;
            Fabrik.Capacity += Buildings[fieldNumber].BuildingInfo.Fabrik;
        }

        public void ChangeCore(string newCore)
        {
            Core.ChangeBuilding(BuildingInfos[newCore]);
        }

        public string GetImgPath(int fieldNumber)
        {
            return Buildings[fieldNumber].GetImgPath();
        }

        public Building[] Buildings { get; private set; }
        public Building Core { get; private set; } 
        public Erzverarbeitung Erzverarbeitung { get; private set; }
        public Fabrik Fabrik { get; private set; } 

        public int MaxUnterirdisch => (int)Math.Floor(Buildings.Length / 5.0d);
        public int NumberOfUnterirdisch { get; private set; } = 0;

        public decimal GetProduktionValue()
        {
            decimal amount = 0;
            foreach (var ress in RessourceInfos.Keys)
            {
                amount += RessourceInfos[ress].Wert * GetRessourceProduction(ress);
            }

            return amount;
        }


        public Dictionary<string, BuildingInfo> BuildingInfos = new Dictionary<string, BuildingInfo>();
        public Dictionary<string, Ressource> RessourceInfos = new Dictionary<string, Ressource>();
    }

    public class Erzverarbeitung
    {
        Dictionary<string, Ressource> _basicProductionCosts;
        public Erzverarbeitung(Dictionary<string, Ressource> productionCosts)
        {
            _basicProductionCosts = productionCosts;
        }

        public int Capacity = 0;
        Dictionary<string, decimal> RessourceProduction = new Dictionary<string, decimal>();
        //Dictionary<string, int> RessourceProductionCosts = new Dictionary<string, int>();

        public decimal Production(string ressource)
        {
            bool ressourceIsInDictionary = RessourceProduction.TryGetValue(ressource, out decimal value);

            return ressourceIsInDictionary ? value : 0;
        }
        public void ChangeProductionValue(string ressourceName, int newAmount)
        {
            var ressource = _basicProductionCosts[ressourceName];

            var hasOldAmount = RessourceProduction.TryGetValue(ressourceName, out decimal oldAmount);
            oldAmount = hasOldAmount ? oldAmount : 0;

            foreach (var ressourceCost in ressource.ErzverarbeitungRessource.Produktion)
            {
                if (RessourceProduction.ContainsKey(ressourceCost.Key))
                {
                    RessourceProduction[ressourceCost.Key] += (newAmount - oldAmount) * ressourceCost.Value;
                }
                else
                {
                    RessourceProduction[ressourceCost.Key] = newAmount * ressourceCost.Value;
                }
            }

            _auslastung += (newAmount - oldAmount) * ressource.ErzverarbeitungRessource.Zeit;
        }

        decimal _auslastung = 0;
        public decimal GetAuslastung()
        {
            return _auslastung;
        }
    }


    public class Fabrik
    {
        Dictionary<string, Ressource> _basicProductionCosts;
        public Fabrik(Dictionary<string, Ressource> productionCosts)
        {
            _basicProductionCosts = productionCosts;
        }

        public int Capacity = 0;
        Dictionary<string, decimal> RessourceProduction = new Dictionary<string, decimal>();
        //Dictionary<string, int> RessourceProductionCosts = new Dictionary<string, int>();

        public decimal Production(string ressource)
        {
            bool ressourceIsInDictionary = RessourceProduction.TryGetValue(ressource, out decimal value);

            decimal amount = 0;
            foreach (var producedItem in RessourceProduction)
            {
                _basicProductionCosts[producedItem.Key].Kosten.TryGetValue(ressource, out decimal val);
                amount += val*producedItem.Value;
            }

            return amount;
            //return ressourceIsInDictionary ? value : 0;
        }
        public void ChangeProductionValue(string ressourceName, int newAmount)
        {
            var ressource = _basicProductionCosts[ressourceName];

            var hasOldAmount = RessourceProduction.TryGetValue(ressourceName, out decimal oldAmount);
            oldAmount = hasOldAmount ? oldAmount : 0;

            RessourceProduction[ressourceName] = newAmount;

            //foreach (var ressourceCost in ressource.Kosten)
            //{
            //    if (RessourceProduction.ContainsKey(ressourceCost.Key))
            //    {
            //        RessourceProduction[ressourceCost.Key] += (newAmount - oldAmount) * ressourceCost.Value;
            //    }
            //    else
            //    {
            //        RessourceProduction[ressourceCost.Key] = newAmount * ressourceCost.Value;
            //    }
            //}



            _auslastung += (newAmount - oldAmount) * ressource.Kosten["Zeit"];
        }

        decimal _auslastung = 0;
        public decimal GetAuslastung()
        {
            return _auslastung;
        }
    }
}
