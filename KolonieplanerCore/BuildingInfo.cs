﻿using System.Collections.Generic;

namespace KolonieplanerCore
{
    public class BuildingInfo
    {
        public string Name;
        public string Kategorie;
        public bool IsUnterirdisch = false;

        public Dictionary<string, int> BuildingCosts = new Dictionary<string, int>();
        public Dictionary<string, int> BuildingProduction = new Dictionary<string, int>();

        public int Erzfabrik = 0;
        public int Fabrik = 0;

        public int GetCostByRessourceName(string ressourceName)
        {
            var exists = BuildingCosts.TryGetValue(ressourceName, out int val);

            return exists ? val : 0;
        }

        public int GetProductionByRessourceName(string ressourceName)
        {
            var exists = BuildingProduction.TryGetValue(ressourceName, out int val);

            return exists ? val : 0;
        }


        public string ImgPath;
    }
}
