﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.Json;

namespace KolonieplanerCore
{

    public class Ressource
    {
        //public Ressource(JsonElement parsedData)
        //{
        //    _parsedData = parsedData;
        //}
        public string Name;
        public string ImgPath;
        public string Kategorie = "Rohstoff";
        public bool IsErz = false;
        public decimal Wert = 0;
        public ErzverarbeitungRessource ErzverarbeitungRessource = new ErzverarbeitungRessource();
        public Dictionary<string, decimal> Kosten = new Dictionary<string, decimal>();

        //private JsonElement _parsedData;
    }

    public class ErzverarbeitungRessource
    {
        //"Erz": -2,
        //"Uran": 1,
        //"Zeit": 0.01
        public Dictionary<string, decimal> Produktion = new Dictionary<string, decimal>();
        public decimal Zeit = 0;
    }
    public class Ressources
    {
        public int Nahrung = 0;
        public int Deuterium = 0;
        public int Kunststoff = 0;
        public int Titan = 0;
        public int Uran = 0;
        public int Antimaterie = 0;
        public int Adamantium = 0;
        public int Platin = 0;
        public int Silizium = 0;
        public int Xentronium = 0;
        public int Erz = 0;
        public int Isochips = 0;
        public int Eis = 0;
        public int Gestein = 0;
        public int Wasser = 0;
        public int Energie = 0;
        public int Einwohner = 0;
        public int Arbeiter = 0;
        //Erz2

        public static Ressources operator +(Ressources res, Ressources res2)
        {
            return new Ressources()
            {
                Nahrung = res.Nahrung + res2.Nahrung,
                Deuterium = res.Deuterium + res2.Deuterium,
                Kunststoff = res.Kunststoff + res2.Kunststoff,
                Titan = res.Titan + res2.Titan,
                Antimaterie = res.Antimaterie + res2.Antimaterie,
                Adamantium = res.Adamantium + res2.Adamantium,
                Platin = res.Platin + res2.Platin,
                Silizium = res.Silizium + res2.Silizium,
                Xentronium = res.Xentronium + res2.Xentronium,
                Erz = res.Erz + res2.Erz,
                Isochips = res.Isochips + res2.Isochips,
                Eis = res.Eis + res2.Eis,
                Gestein = res.Gestein + res2.Gestein,
                Wasser = res.Wasser + res2.Wasser,
                Energie = res.Energie + res2.Energie,
                Einwohner = res.Einwohner + res2.Einwohner,
                Arbeiter = res.Arbeiter + res2.Arbeiter
            };
        }
    }
}
