﻿namespace KolonieplanerCore
{
    public class Building
    {
        public BuildingInfo BuildingInfo;
        public readonly int Id;

        bool IsActive = true;

        public Building(int id, BuildingInfo buildingInfo = null)
        {
            Id = id;
            BuildingInfo = buildingInfo;

            if (BuildingInfo == null)
            {
                BuildingInfo = new BuildingInfo() 
                { 
                    Name = "Empty", 
                    ImgPath = "./img/buildings/ground0.png"
                };
            }
        }
        public string GetImgPath()
        {
            return BuildingInfo.ImgPath;
        }

        public int GetProduction(string ressourceName)
        {
            if (IsActive)
            {
                return BuildingInfo.GetProductionByRessourceName(ressourceName);
            }
            else return 0;
        }
        public int GetCosts(string ressourceName)
        {
            return BuildingInfo.GetCostByRessourceName(ressourceName);
        }

        public void ChangeBuilding(BuildingInfo buildingInfo)
        {
            if (Id != 0)
            {
                BuildingInfo = buildingInfo;
            }
        }
    }
}
