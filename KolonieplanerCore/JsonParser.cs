﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;

namespace KolonieplanerCore
{
    public class JsonParser
    {
        public Dictionary<string, BuildingInfo> BuildingInfos = new Dictionary<string, BuildingInfo>();
        public Dictionary<string, Ressource> Ressources = new Dictionary<string, Ressource>();
        public JsonParser()
        {
            ParseRessources();
            ParseBuildings();
        }

        private void ParseRessources()
        {
            string jsonPath = "./Ressources.json";

            var test = File.ReadAllText(jsonPath);
            var document = JsonDocument.Parse(test);

            JsonElement root = document.RootElement;
            JsonElement BuildingsArray = root.GetProperty("Ressources");
            foreach (JsonElement ressource in BuildingsArray.EnumerateArray())
            {
                var name = ressource.GetProperty("Name").GetString();
                var imgPath = ressource.GetProperty("ImgPath").GetString();
                var isErz = ressource.TryGetProperty("Erz", out var isErzVal);
                var hasKosten = ressource.TryGetProperty("Kosten", out var val);
                var wert = ressource.GetProperty("Wert").GetDecimal();
                var kategorie = ressource.GetProperty("Kategorie").GetString();
                var produktionkostenList = new Dictionary<string, decimal>();

                if (hasKosten)
                {
                    var kosten = ressource.GetProperty("Kosten");

                    foreach (var produktionsKosten in Ressources.Values)
                    {
                        var propertyExists = kosten.TryGetProperty(produktionsKosten.Name, out var jsonProduktionskosten);

                        if (propertyExists)
                        {
                            var amount = jsonProduktionskosten.GetDecimal();

                            produktionkostenList.Add(produktionsKosten.Name, amount);
                        }
                    }

                    var self = kosten.TryGetProperty(name, out var selfValue);
                    if (self && !produktionkostenList.ContainsKey(name))
                    {
                        produktionkostenList.Add(name, selfValue.GetDecimal());
                    }
                }

                var ress = new Ressource()
                {
                    Name = name,
                    ImgPath = imgPath,
                    IsErz = isErz,
                    Wert = wert,
                    Kategorie = kategorie,
                    Kosten = produktionkostenList
                };

                Ressources.Add(name,
                    ress);

                if (isErz)
                {
                    var amountRess = val.GetProperty(name).GetInt32();
                    var zeit = val.GetProperty("Zeit").GetDecimal();

                    ress.ErzverarbeitungRessource.Produktion.Add("Erz", produktionkostenList["Erz"]);
                    ress.ErzverarbeitungRessource.Produktion.Add(name, amountRess);
                    ress.ErzverarbeitungRessource.Zeit = zeit;
                }
            }
        }

        private void ParseBuildings()
        {
            string jsonPath = "./Buildings.json";

            var test = File.ReadAllText(jsonPath, Encoding.GetEncoding("iso-8859-1"));
            var document = JsonDocument.Parse(test);

            JsonElement root = document.RootElement;
            JsonElement BuildingsArray = root.GetProperty("Buildings");
            
            foreach (JsonElement building in BuildingsArray.EnumerateArray())
            {
                var info = GetBuildingInfo(building);
                BuildingInfos.Add(info.Name, info);
            }
        }

        public BuildingInfo GetBuildingInfo(JsonElement building)
        {
            var buildingInfo = new BuildingInfo();

            buildingInfo.Name = building.GetProperty("Name").GetString();
            buildingInfo.ImgPath = building.GetProperty("ImgPath").GetString();
            buildingInfo.Kategorie = building.GetProperty("Kategorie").GetString();

            var costs = building.GetProperty("Kosten");
            var production = building.GetProperty("Produktion");

            foreach (var ressource in Ressources.Keys)
            {
                bool ressourceExists = costs.TryGetProperty(ressource, out var val);
                buildingInfo.BuildingCosts[ressource] = ressourceExists ? val.GetInt32() : 0;


                ressourceExists = production.TryGetProperty(ressource, out val);
                buildingInfo.BuildingProduction[ressource] = ressourceExists ? val.GetInt32() : 0;
            }

            var hasSonstige = building.TryGetProperty("Sonstiges", out var sonstiges);

            if (hasSonstige)
            {
                var isErz = sonstiges.TryGetProperty("Erz", out var erzfabrikVal);
                if (isErz)
                {
                    erzfabrikVal.TryGetInt32(out buildingInfo.Erzfabrik);
                }

                var isFabrik = sonstiges.TryGetProperty("Fabrik", out var fabrikValue);
                if (isFabrik)
                {
                    buildingInfo.Fabrik = fabrikValue.GetInt32();
                }

                var isUnterirdisch = sonstiges.TryGetProperty("Unterirdisch", out var unterirdischVal);
                if (isUnterirdisch)
                {
                    buildingInfo.IsUnterirdisch = unterirdischVal.GetBoolean();
                }
            }

            return buildingInfo;
        }

        public Building GetBuilding()
        {
            return null;
        }
    }
}
